package expressions;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Regex {

    public static void main(String[] args) {
        // String regexPattern = "^([^+]{3})\\.([^+]{3})\\.([^+]{3})\\.([^+]{3})$";
        // String regexPattern = "...\\....\\....\\...."
        // String regexPattern = ".{3}\\..{3}\\..{3}\\..{3}";
        // does not work - String regexPattern = "[.]{3}\\.[.]{3}\\.[.]{3}\\.[.]{3}";
        // String regexPattern = "\\S\\S\\S\\.\\S\\S\\S\\.\\S\\S\\S\\.\\S\\S\\S";
        // String regexPattern = "\\S{3}\\.\\S{3}\\.\\S{3}\\.\\S{3}";
        String regexPattern = "[\\S]{3}\\.[\\S]{3}\\.[\\S]{3}\\.[\\S]{3}";

        Pattern p = Pattern.compile(regexPattern);
        Matcher m = p.matcher("123.132.123.123");
        //Matcher m = p.matcher("`!@.#$%.^&*.()_");
        //Matcher m = p.matcher("...............");
        boolean match = m.matches();
        System.out.println(match);
        System.out.println(m.replaceAll("xxx"));
    }
}
